$(function() {
    $('.ui.button')
        .on('click', function() {
            var
                $progress       = $('#example1'),
                $button         = $(this),
                updateEvent
            ;
            // restart to zero
            clearInterval(window.fakeProgress)
            $progress.progress('reset');
            // updates every 10ms until complete
            window.fakeProgress = setInterval(function() {
                $progress.progress('increment');
                $button.text( $progress.progress('get value') );
                // stop incrementing when complete
                if($progress.progress('is complete')) {
                    clearInterval(window.fakeProgress)
                }
            }, 10);
        })
    ;
    $('.ui.progress')
        .progress({
            // duration : 100,
            // total    : 100,
            text     : {
                active: '{value} of {total} done'
            }
        })
    ;



    $("input:text").click(function() {
        $(this).parent().find("input:file").click();
    });

    $('input:file', '.ui.action.input')
        .on('change', function(e) {
            var name = e.target.files[0].name;
            $('input:text', $(e.target).parent()).val(name);
        });



    $('.ui.dropdown')
        .dropdown({
            allowAdditions: true
        })
    ;

    var mode = $('#attack_mode');
    var mask = $('#mask');
    var wordlist = $('#wordlist');
    var wordlist2 = $('#wordlist2');

    mode.change(function() {
      var value = this.value;
      console.log(value);
      if (value == '3') {
        mask.fadeIn();
        wordlist.addClass('hidden');
      }
      if (value == '1') {
        wordlist2.removeClass('hidden')
      }
      if (value == '6') {
        mask.fadeIn();
      }
      else {
        mask.addClass('hidden');
        wordlist2.addClass('hidden');
      }
    });
    
    
});


