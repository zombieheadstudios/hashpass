# frozen_string_literal: true

module Hashpass
  ROOT = File.expand_path('..', __FILE__)
end

$LOAD_PATH.unshift Hashpass::ROOT

require 'config/application'

run Hashpass::Application
