module Hashpass
  module Routes
    module App
      NotFound = Syro.new(Decks::App) do
        default do
          render 'not_found'
        end
      end
    end
  end
end