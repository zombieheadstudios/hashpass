module Hashpass
  module Routes
    module App
      Core = Syro.new(Decks::App) do
        get do
          res.redirect '/job-create'
        end

        on 'job-list' do
          render 'job_list'
        end

        on 'kill-all' do
          get do
            Jobs.kill_all
            res.redirect '/job-list'
          end
        end

        on 'output-jobs' do
          get do
            Jobs.output
            res.redirect '/job-list'
          end
        end

        # TODO: Refactor the crap out of this
        on 'job-create' do
          get do
            @ruleFiles = Dir['rules/**/*.rule']
            @wordlistFiles = Dir['dics/**/*']
            render 'job_create'
          end

          post do
            # Get input paramaters
            @wordlist = req.params['wordlist']
            @wordlist2 = req.params['wordlist2']
            @name = req.params['name']
            @attack_mode = req.params['attack_mode']
            @hash_mode = req.params['hash_mode']
            @mask = req.params['mask']
            @rules = req.params['rules']
            @tempfile = req.params['file'][:tempfile] || 'tempfile'
            @filename = req.params['file'][:filename] || 'something'
            @job_type = req.params['job_type']

            puts "Filename: #{@filename}"
            puts "job type: #{@job_type}"

            # Upload and Convert file
            if @hash_mode == '2500'
              FileUtils.mv(@tempfile, 'tmp/hashes/' + @filename)
              cap2hccapx(@filename)
              simple_params = {
                hash_mode: @hash_mode,
                attack_mode: @attack_mode,
                mask: @mask,
                rules: @rules,
                hash_file: @filename + '.hccapx',
                dictionary: @wordlist,
                dictionary2: @wordlist2
              }
              p cmd = @hcat.build_cmd(simple_params)
              if @job_type == 'queue'
                Jobs.queue(cmd, @filename + '.hccapx')
              else
                Jobs.run_job(cmd, @filename + '.hccapx')
              end
            else
              FileUtils.mv(@tempfile, 'tmp/hashes/' + @filename)
              simple_params = {
                hash_mode: @hash_mode,
                attack_mode: @attack_mode,
                mask: @mask,
                rules: @rules,
                hash_file: @filename,
                dictionary: @wordlist,
                dictionary2: @wordlist2
              }
              p cmd = @hcat.build_cmd(simple_params)
              Jobs.run_job(cmd, @filename)
            end

            res.redirect '/job-list'
          end
        end
      end
    end
  end
end
