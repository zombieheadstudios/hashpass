module Hashpass
  module Decks
    ##
    # Main hashcat application
    #
    class App < Syro::Deck
      MOUNT_POINT = '/'.freeze
      VIEWS_DIRECTORY = 'app'.freeze

      include Helpers::Rendering
      include Helpers::Util
      include Helpers::Hashcat

      def call(env, inbox = {})
        super(env, inbox)
      end
    end
  end
end
