# frozen_string_literal: true

require 'bundler'

# Require our default gems
Bundler.require(:default, ENV['RACK_ENV'])

# TODO: Add basic docs
# TODO: Feature: add masks directory as dropdown option
# TODO: remove unused
require 'pry'
require 'fileutils'
require 'helpers/hashcat'
require 'helpers/rendering'
require 'helpers/util'
require 'lib/jobs'
require 'middleware/not_found'
require 'decks/app'
require 'routes/app/core'
require 'routes/app/not_found'
require 'routes/app/login'

# TODO: put these global things in better place
trap 'SIGINT' do
  puts 'Exiting'
  Jobs.kill_all
  clean
  exit 130
end

def clean
  puts 'cleaning...'
  `rm -rf tmp`
  `mkdir tmp`
  `mkdir tmp/hashes`
  `mkdir tmp/stdout`
end

module Hashpass
  Application = Rack::Builder.new do
    use Rack::Static,
        root: 'public',
        urls: %w[/images /scripts /styles]

    use Middleware::NotFound do |env|
      Routes::App::NotFound.call(env)
    end

    # TODO: unused? remove
    map '/login' do
      run Routes::App::Login
    end

    # TODO: add API

    run Routes::App::Core
  end
end
