##
# Single job to be ran on command line
#
class Job
  attr_reader :pid, :name

  # TODO: Fix this so it returns correct value
  STATUS = {
    0 => 'Init',
    1 => 'Auto Tune',
    2 => 'Self Test',
    3 => 'Running',
    4 => 'Paused',
    5 => 'Exhausted',
    6 => 'Cracked',
    7 => 'Aborted',
    8 => 'Quit',
    9 => 'Bypass',
    10 => 'Aborted Checkpoint',
    11 => 'Aborted Runtime'
  }
  # STATUS.default = 'Unknown'

  def initialize(pid, name)
    @pid = pid
    @name = name
  end

  def running?
    Process.getpgid(pid)
    true
  rescue Errno::ESRCH
    false
  end

  def which_status(code)
    # puts "Status code: #{code}"
    STATUS[code].to_s
  end

  def status_code
    val = status('STATUS', 1)
    which_status(val)
  end

  def status(search_word, selection_index)
    tail = `tail -n 3 tmp/stdout/#{name}`.split("\t")
    return if tail.empty? || tail.length < 3
    word_index = tail.index(search_word) || 0
    selection = word_index + selection_index
    tail[selection]
  end
end

##
# Holds list of all the current running command
# line jobs
#
class Jobs
  @@jobs = []
  @@queue = []

  ##
  # Get the list of current jobs
  #
  # @retruns jobs [Array] list of current running jobs
  def self.jobs
    jobs = []
    @@jobs.each do |job|
      jobs << job if job.running?
    end
    jobs
  end

  ##
  # Get the list of current jobs
  #
  # @retruns jobs [Array] list of current running jobs
  def self.queues
    queues = []
    @@queue.each do |queue|
      queues << queue if queue.running?
    end
    queues
  end

  ##
  # Add a job to thread
  #
  # @params job options [Hash] details of the job we're creating
  # @returns pid [Fixnum] process identifier
  def self.run_job(cmd, name)
    pid = Process.spawn(cmd)
    @@jobs << Job.new(pid, name)
    pid
  end

  ##
  # Add a job to the queue
  #
  # @params job options [Hash] details of the job we're creating
  # @returns pid [Fixnum] process identifier
  def self.queue(cmd, name)
    pid = Process.spawn(cmd) #if @@queue.empty?
    @@queue.push Job.new(pid, name)
    pid
  end

  ##
  # Kill a job (process)
  #
  # @params job [Fixnum] process id (pid)
  def self.kill_job(pid)
    Process.kill(9, pid)
  end

  ##
  # Kill all the processes!
  def self.kill_all
    @@jobs.each do |job|
      Process.kill(9, job.pid)
    end
  end

  def self.completed_jobs
    out = []
    Dir.glob('cracked/*.crack') do |file|
      File.readlines(file).each do |line|
        out << line
      end
    end

    out
  end

  ##
  ## Output the jobs to the console
  def self.output
    @@jobs.each do |job|
      puts "Job: #{job.inspect}"
    end
  end
end
