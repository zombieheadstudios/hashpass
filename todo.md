- [ ] Queues
- [ ] Pause/Restart/Stop Jobs
- [ ] Remove name
- [ ] ETA
- [ ] Enable/Disable Autoreload 
- [ ] API to accept hashes
- [ ] SMS messaging system
- [ ] Attack Types - NTLM etc
- [ ] Docs
- [ ] TDD
- [ ] Auth
- [ ] Database integration
- [ ] Users
- [ ] Permissions
- [ ] Refactoring & cleanup

- [ ] Read stdout from cli
- [ ] Display progress on front-end
- [ ] Stop a job
- [ ] Resume a job
- [ ] Display results
- [ ] Choose hash type
- [ ] MySQL integration
- [ ] Save results to db
- [ ] User object
- [ ] Authorization. Simple?
- [ ] Script to upload pcap from wifite