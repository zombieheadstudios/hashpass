# encoding: UTF-8

# frozen_string_literal true

module Hashpass
  module Middleware
    ##
    # 404 Middleware
    #
    # There will be awesome 404nesss
    #
    # Usage:
    #
    #   use Middleware::NotFound do |env|
    #        RouteToHandle404.call(env)
    #   end
    #
    class NotFound
      def initialize(app, &default)
        @app = app
        @default = default
      end

      def call(env)
        # get the response from the underlying app
        status, headers, body = @app.call(env)

        # Capture empty 404 responses and return a generic page
        if status == 404 && body.empty?
          @default.call(env)
        else
          [status, headers, body]
        end
      end
    end
  end
end
