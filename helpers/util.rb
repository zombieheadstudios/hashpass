module Hashpass
  module Helpers
    # Hashcat Terminal utility functions
    module Util
      def cap2hccapx(filename)
        Thread.new do
          cap2hccapx = './tools/cap2hccapx-mac.bin'
          cap_location = "tmp/hashes/#{filename}"
          hcap = "tmp/hashes/#{filename}.hccapx"
          convert_cmd = "#{cap2hccapx} #{cap_location} #{hcap}"
          puts "- Converting: #{convert_cmd}"
          system(convert_cmd)
        end
      end

      # TODO: add documenation
      def create_thread(cmd)
        t = Thread.new do
          IO.popen(cmd, 'r+') do |hashcat|
            hashcat.each do |line|
              puts line
              @hashcat_output = line
            end
          end
        end
      end
    end
  end
end
