module Hashpass
  module Helpers
    module Rendering
      # TODO: Remove unused, understand this fully

      # Initializing this to enable template creation
      TEMPLATE_CLASS = Tilt::ErubiTemplate

      # The instance variable that rendered content will be saved to.
      TILT_OUTVAR = '@_outvar'

      # Option passed to Tilt.
      TILT_OPTIONS = { outvar: TILT_OUTVAR, escape: true, freeze: true, default_encoding: Encoding::UTF_8 }.freeze

      #
      # A parsed tilt template
      #
      def template(path)
        TEMPLATE_CLASS.new(path, TILT_OPTIONS)
      end
      module_function :template

      #
      # Render a template
      #
      def partial(path, locals = {}, &content)
        root = locals.delete(:root) { 'views' }
        from = locals.delete(:from) { self.class::VIEWS_DIRECTORY }

        template("#{ root }/#{ from }/#{ path }.erb").render(self, locals, &content)
      end
      module_function :partial

      #
      # Wraps the return from partial and writes output to response as HTML
      #
      def render(path, locals = {}, &content)
        body = partial(path, locals.dup, &content)
        layout = locals.delete(:layout) { 'default' }

        # Write the content out with the layout as HTML.
        res.html(partial("layout/#{ layout }", locals.merge(from: self.class::VIEWS_DIRECTORY)) { body })
      end
    end
  end
end