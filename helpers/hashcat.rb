require 'models/hashcat'

module Hashpass
  module Helpers
    ##
    # Builds options for running hashcat from the
    # command line.
    #
    module Hashcat
      def call(env, inbox)
        @hcat = Models::Hashcat.new
        super(env, inbox)
      end
    end
  end
end
