$LOAD_PATH.unshift File.expand_path('..', __FILE__)
ENV['RACK_ENV'] = 'test'

require 'bundler'
require 'minitest/autorun'
require 'meme'

# Test awesome meme class
class TestMeme < Minitest::Test
  def setup
    @meme = Meme.new
  end

  def test_that_kitty_can_eat
    assert_equal('OHAI!', @meme.i_can_has_cheezburger?)
  end

  def test_that_something_happens
    assert_match(/Sure/, @meme.something?)
  end

  def test_is_it_awesome
    assert_equal('yup!', @meme.awesome?)
  end

  def test_that_it_will_not_blend
    refute_match(/^no/i, @meme.will_it_blend?)
  end

  def test_awesome_is_always_awesome
    refute_match(/no/, @meme.awesome?)
  end

  def test_that_will_be_skipped
    skip 'test this later'
  end
end

