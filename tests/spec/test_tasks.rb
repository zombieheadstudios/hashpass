$LOAD_PATH.unshift File.expand_path('..', __FILE__)
ENV['RACK_ENV'] = 'test'

require 'bundler'
require 'minitest/autorun'
require_relative '../../tasks'

describe Tasks do
  before do
    @tasks = Tasks.instance
  end

  describe 'when asked to find percentage in a string' do
    it 'must respond with a percentage value' do
      actual_value = 'I should find this 99.3% percentage'
      expected_value = '99.3'
      @tasks.find_percentage(actual_value).must_equal expected_value
    end
  end

  # TODO: assert opening a file
  describe 'when asked to open a hashcat log file' do
    it 'must successfully load the file' do
      skip 'test this later'
      expected_value = nil
      @tasks.read_hashcat_log('test').must_equal expected_value
    end
  end
end
