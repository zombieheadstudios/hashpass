$LOAD_PATH.unshift File.expand_path('..', __FILE__)
ENV['RACK_ENV'] = 'test'

require 'bundler'
require 'minitest/autorun'
require_relative '../tasks'

# TODO: Add some tests man!

# Test awesome meme class
class TestTasks < Minitest::Test
  def setup
    @tasks = Tasks.instance
  end

  def test_can_find_percentage
    assert_equal('99.3', @tasks.find_percentage('I should find 99.3% second is 23.4'))
  end

  def test_can_read_log_file
    assert_equal('--', @tasks.read('test'))
  end
end
