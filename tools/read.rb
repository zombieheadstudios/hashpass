require 'pty'
cmd = "hashcat -t 32 -a 7 upload/example0.hash ?a?a?a?a upload/example.dict -o outfile.txt" 
begin
  PTY.spawn( cmd ) do |stdout, stdin, pid|
    begin
      # Do stuff with the output here. Just printing to show it works
      stdout.each do |line| 
        print line 
      end

      stdin.puts('p')

    rescue Errno::EIO
      puts "Errno:EIO error, but this probably just means " +
            "that the process has finished giving output"
    end
  end
rescue PTY::ChildExited
  puts "The child process exited!"
end