module Hashpass
  module Models
    ##
    # Builds options for running hashcat from the
    # command line.
    #
    class Hashcat
      # TODO: add these to application configuration?
      attr_reader :cmd
      HASH_DIR =      'tmp/hashes/'.freeze
      STDOUT_DIR =    'tmp/stdout/'.freeze
      CRACKED_DIR =   'cracked/'.freeze
      DICS_DIR =      'dics/'.freeze
      RULES_DIR =     'rules'.freeze
      DICS = {
        'all' => 'dics/',
        'john' => 'dics/john.txt',
        'phpbb' => 'dics/phpbb.txt',
        'rockyou' => 'dics/large/rockyou.txt',
        'netgear' => 'dics/large/NetgearKiller.txt',
        'm3g9tr0n' => 'dics/m3g9tr0n/',
        'crackstation' => 'dics/crackstation.txt',
        'crackstation-human' => 'dics/large/crackstation-human.txt'
      }.freeze

      ##
      # Returns a string with all user options for cracking
      # the hash file
      #
      # @param options [Hash] insert values for things such
      #  as attack_mode, dictionary, hash_file and hash_mode
      # @return [String] command to be used with run_cmd
      def build_cmd(options = {})
        attack_mode = options.fetch(:attack_mode) { '0' }
        dictionary =  options.fetch(:dictionary) { '' }
        dictionary2 = options.fetch(:dictionary2) { '' }
        hash_file =   options.fetch(:hash_file) { '' }
        hash_mode =   options.fetch(:hash_mode) { '2500' }
        mask =        options.fetch(:mask) { '' }
        rules =       options.fetch(:rules) { '' }
        
        @cmd = "hashcat -m #{hash_mode} -a #{attack_mode} -D 1 "\
          "--machine-readable --status --status-timer=1 #{rules} "\
          "#{HASH_DIR}#{hash_file} #{dictionary} #{dictionary2} #{mask} "\
          " -o #{CRACKED_DIR}#{hash_file}.crack > #{STDOUT_DIR}#{hash_file}"
      end
    end
  end
end
